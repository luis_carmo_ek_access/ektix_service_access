<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 22-12-2017
 * Time: 17:49
 */

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;


class AccessController extends Controller
{

	private $service_access_validation;

    public function __construct()
    {
        $this->middleware('auth');
				$this->service_access_validation = env('SERVICE_ACCESS_VALIDATION', 'http://127.0.0.1:4444/');
    }

    public function validateTicketAccess(Request $request, $event_code, $barcode)
    {

        $header = array(
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8'
        );

        try {

            $request_data = array(
            	'host' => $this->service_access_validation,
	            'username' => $request->getUser(),
	            'password' => $request->getPassword(),
	            'verify_certificate' => 0
            );

            $access_response = $this->requestAccess($request_data, $event_code, $barcode);

            return response()->json($access_response, 200, $header, JSON_UNESCAPED_UNICODE);

        } catch (Exception $e) {

            return response()->json(array('errors' => array('error' => 'Internal Server Error')), 500);

        }

    }

    private function requestAccess($request_data, $event_code, $barcode) {

        try {

            $client = new Client(['base_uri' => $request_data['host']]);

            $response = $client->request(
                'POST',
                $request_data['host'] . 'trans_api/trans_event/',
                [
                	'auth' => [$request_data['username'], $request_data['password']],
	                'connect_timeout' => 1,
	                'json' => ['evento' => $event_code, 'titulo' => $barcode]
                ]
            );

            if ($response->getStatusCode() === 200) {

                $responseAccess = $response->getBody();

                if (isset($responseAccess) && !empty($responseAccess)) {

                    $responseAccess = json_decode($responseAccess, TRUE);
                    $entry = array(
                        'entry' => isset($responseAccess['entry']) ? $responseAccess['entry'] : null
                    );

                    return $entry;

                } else {

                    return array('error' => 'Response Unknown!');

                }


            } else {

	            return array('error' => 'Connection Timeout!');

            }

        } catch (RequestException $e) {

            return array('error' => 'Internal Server Error');

        }

    }

    public function  getHistoricBarcode(Request $request, $barcode) {

        $header = array(
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8'
        );

	      $uri_parts = explode('?', $request->fullUrl());

	      $uri_params = array_slice($uri_parts, 1);

        try {

            $request_data = array(
                'host' => $this->service_access_validation,
                'username' => $request->getUser(),
                'password' => $request->getPassword(),
                'verify_certificate' => 0
            );

            $transactions_response = $this->requestTransactions($request_data, isset($uri_params[0]) ? $uri_params[0] : $uri_params, $barcode);

            return response()->json($transactions_response, 200, $header, JSON_UNESCAPED_UNICODE);

        } catch (Exception $e) {

            return response()->json(array('errors' => array('error' => 'Internal Server Error')), 500);

        }

    }

    private function requestTransactions($request_data, $uri_params, $barcode) {

    	$params = !empty($uri_params) ? '/?' . $uri_params : '/';

        try {

            $client = new Client(['base_uri' => $request_data['host']]);

            $response = $client->request(
                'GET',
                $request_data['host'] . 'trans_api/transactions/' . $barcode . $params,
                [
                    'auth' => [$request_data['username'], $request_data['password']],
                    'connect_timeout' => 10
                ]
            );

            if ($response->getStatusCode() === 200) {

                $responseAccess = $response->getBody();

                if (isset($responseAccess) && !empty($responseAccess)) {

                    $responseAccess = json_decode($responseAccess, TRUE);

                    return $responseAccess;

                } else {

                    return array('error' => 'Response Unknown!');

                }

            } else {

                return array('error' => 'Connection Timeout!');

            }

        } catch (RequestException $e) {

            return array('error' => 'Internal Server Error');

        }

    }

}